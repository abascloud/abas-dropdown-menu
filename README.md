# abas-dropdown-menu

Dropdown menu with icon and text button and a listbox with items with icon and text

## Install the Polymer-CLI

First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) installed. Then run `polymer serve` to serve your element locally.

## Viewing Your Element

```
$ polymer serve
```

## Running Tests

```
$ polymer test
```

## Usage
```html
<abas-dropdown-menu title="Click me!" picture-source="images/image.jpg" items=[[items]]></abas-dropdown-menu>
<abas-dropdown-menu title="Click me!" icon="menu" items=[[items]]></abas-dropdown-menu>
```
```javascript
items = [{"text": "Item 1", "icon": "menu", "action": "action1"},{"text":"Item 2"}];
```
`noink` controls whether there is a ripple-effect on the button.
`raised` controls whether the button looks raised or not.

If you specify both `picture-source` and `icon` both will appear.
When you click an item it will fire the event `abas-dropdown-menu-item-clicked` with the action you specified in the `action` property for the entry, or `undefined` if you didn't do that.

### Styling
The following custom properties and mixins are available for styling:

| *Custom Property*                              | *Description*                                                                         | *Default value* |
|------------------------------------------------|---------------------------------------------------------------------------------------|-----------------|
| `--abas-dropdown-menu-button-background-color` | The background color of the button that opens the dropdown                            | `white`         |
| `--abas-dropdown-menu-button-color`            | The main color of the button that opens the dropdown - for the icon and title         | `black`         |
| `--abas-dropdown-menu-button-min-height`       | The minimum height of the button that opens the dropdown                              | `48px`          |
| `--abas-dropdown-menu-item-min-height`         | The minimum height of an item inside the dropdown. Binds to `--paper-item-min-height` | `48px`          |
| `--abas-dropdown-menu-dropdown-max-width`      | The maximum width of the dropdown                                                     | `300px`         |
| `--abas-dropdown-menu-button-max-width`        | The maximum width of the button that opens the dropdown                               | `initial`       |


### Demo
```
demo/index.html
```
